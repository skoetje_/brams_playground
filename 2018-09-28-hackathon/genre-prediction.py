# main :
# https://www.imsdb.com

# https://www.springfieldspringfield.co.uk/episode_scripts.php?tv-show=13-reasons-why-2017
# https://github.com/alberanid/imdbpy/tree/master/docs


import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
import string
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split


def load_genres():

    return ['Action',
    'Adventure',
    'Animation',
    'Comedy',
    'Crime',
    'Drama',
    'Family',
    'Fantasy',
    'Film - Noir',
    'Horror',
    'Musical',
    'Mystery',
    'Romance',
    'Sci - Fi',
    'Short',
    'Thriller',
    'War',
    'Western']

def create_dataset(genre1, genre2):

    target_data = pd.read_csv('data/genres.csv')
    input_data = pd.read_csv('data/dataset.csv')

    # for now only comedy and action
    action = target_data[target_data['Genre'] == genre1]
    comedy = target_data[target_data['Genre'] == genre2]
    target_data = pd.concat([action, comedy])
    target_data = target_data.drop_duplicates('Title', keep = False)


    dataset = [input_data, target_data]
    dataset = pd.merge(input_data, target_data, how = 'left', on = ['Title', 'Title'])
    dataset = dataset[pd.notnull(dataset['Genre'])]
    dataset = dataset[pd.notnull(dataset['Text'])]
    X = dataset['Text']
    y = dataset['Genre']
    titles = dataset['Title']

    return X, y, titles


def load_txt(path):

    with open(path, 'r') as file:
        data = file.read().replace('\n', '')

    return data


def string_to_list(str):

    return str.split(" ",str.count(" "))


def tokenize(str):

    stop = stopwords.words('english') + list(string.punctuation)
    return [i for i in word_tokenize(str.lower()) if i not in stop]



def get_all_unique_words():

    pass


def tf_idf():

    pass


def load_test_corpus():
    romeo = load_txt('data/dummy/romeo-juliet.txt')
    ace = load_txt('data/dummy/ace-ventura.txt')
    ten_things = load_txt('data/dummy/10-things.txt')
    airplane = load_txt('data/dummy/airplane.txt')

    corpus = [airplane, ace, romeo, ten_things]
    y = [0, 0, 1, 1]
    return corpus, y




if __name__ == "__main__":


    #nltk.download('stopwords')
    #nltk.download('punkt')

    # Thriller - Romance 0.72
    # Drama - Thriller 0.73
    # Thriller - Action 0.73
    # Comedy - Horror 0.85
    # Sci-Fi - Horror  0.81
    # Sci-Fi - Comedy 0.83



    for i in range(0, 5):
        print('Run {}:'.format(i))
        X, y, titles = create_dataset('Sci-Fi', 'Comedy')
        y = pd.Series(y).astype('category')
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15)


        count_vect = CountVectorizer(stop_words='english')
        X_train_counts = count_vect.fit_transform(X_train)

        from sklearn.feature_extraction.text import TfidfTransformer

        tfidf_transformer = TfidfTransformer()
        X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)


        from sklearn.naive_bayes import MultinomialNB

        clf = MultinomialNB().fit(X_train_tfidf, y_train)

        from sklearn.pipeline import Pipeline

        text_clf = Pipeline([('vect', CountVectorizer()),
                             ('tfidf', TfidfTransformer()),
                             ('clf', MultinomialNB()),
                             ])
        text_clf = text_clf.fit(X_train, y_train)
        predicted = text_clf.predict(X_test)

        print('Naive Bayes:')
        #print(np.mean(predicted == y_test))

        """
        from sklearn.linear_model import SGDClassifier

        text_clf_svm = Pipeline([('vect', CountVectorizer()),
                                 ('tfidf', TfidfTransformer()),
                                 ('clf-svm', SGDClassifier(loss='hinge',
                                                           penalty='l2',
                                                           alpha=1e-3,
                                                           n_iter=5, random_state=42)),
                                 ])
        _ = text_clf_svm.fit(X_train, y_train)
        predicted_svm = text_clf_svm.predict(X_train)
        #print("Support Vector Machine:")
        #print(np.mean(predicted_svm == y_test))
        """
        from sklearn.model_selection import GridSearchCV

        parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
                      'tfidf__use_idf': (True, False),
                      'clf__alpha': (1e-2, 1e-3),
                      }
        gs_clf = GridSearchCV(text_clf, parameters, n_jobs=-1)
        gs_clf = gs_clf.fit(X_train, y_train)

        print(gs_clf.best_score_)
        #print(gs_clf.best_params_)


    exit()

    import matplotlib.pyplot as plt

    from sklearn.naive_bayes import GaussianNB
    from sklearn.linear_model import LogisticRegression
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.svm import LinearSVC
    from sklearn.calibration import calibration_curve

    lr = LogisticRegression(solver='lbfgs')
    gnb = GaussianNB()
    svc = LinearSVC(C=1.0)
    rfc = RandomForestClassifier(n_estimators=100)

    for clf, name in [(lr, 'Logistic'),
                      (gnb, 'Naive Bayes'),
                      (svc, 'Support Vector Classification'),
                      (rfc, 'Random Forest')]:

        clf.fit(X_train, y_train)

        if hasattr(clf, "predict_proba"):
            prob_pos = clf.predict_proba(X_test)[:, 1]
        else:  # use decision function
            prob_pos = clf.decision_function(X_test)
            prob_pos = \
                (prob_pos - prob_pos.min()) / (prob_pos.max() - prob_pos.min())
        fraction_of_positives, mean_predicted_value = \
            calibration_curve(y_test, prob_pos, n_bins=10)

        ax1.plot(mean_predicted_value, fraction_of_positives, "s-",
                 label="%s" % (name,))

        ax2.hist(prob_pos, range=(0, 1), bins=10, label=name,
                 histtype="step", lw=2)

    ax1.set_ylabel("Fraction of positives")
    ax1.set_ylim([-0.05, 1.05])
    ax1.legend(loc="lower right")
    ax1.set_title('Calibration plots  (reliability curve)')

    ax2.set_xlabel("Mean predicted value")
    ax2.set_ylabel("Count")
    ax2.legend(loc="upper center", ncol=2)

    plt.tight_layout()
    plt.show()
    exit()








    genres = load_genres()

    df = pd.read_csv('data/small.csv')

    df = df['Field1']
    print(df)

    corpus = df

    vectorizer = TfidfVectorizer(stop_words='english')

    X = vectorizer.fit_transform(corpus).toarray()

    indices = np.argsort(vectorizer.idf_)[::-1]
    features = vectorizer.get_feature_names()
    top_n = 10
    top_features = [features[i] for i in indices[:top_n]]

    for index, topic in enumerate(corpus):

        current_row = X[index]
        indices_best = current_row.argsort()[-top_n:][::-1]

        for i in indices_best:
            print(features[i])

        print("\n \n")







    """
    Work flow
    
    import all data
    
    each string = X
    
    get label --> Y
    
    build TF-IDF
    
    PCA on features, plot (each genre is a color, plot individual movies)
    
    Split train, test data
    
    Classifier
    
    Plot results
    
    
    
    """